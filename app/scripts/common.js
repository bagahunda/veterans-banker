$(document).ready(function () {

	// 'use strict';

	if($('.compare__table').length) {

		var compare_list_height = $('.compare__table .attr-col li');
		$('.compare__table .ft-special li').each(function( index ) {
			$(this).css('height', parseFloat($(compare_list_height[index]).outerHeight()) + 'px');
		});
		$('.compare__table .ft-body li').each(function( index ) {
			$(this).css('height', parseFloat($(compare_list_height[index]).outerHeight()) + 'px');
		});

		$(window).resize(function() {
			var compare_list_height = $('.compare__table .attr-col li');
			$('.compare__table .ft-special li').each(function( index ) {
				$(this).css('height', parseFloat($(compare_list_height[index]).outerHeight()) + 'px');
			});
			$('.compare__table .ft-body li').each(function( index ) {
				$(this).css('height', parseFloat($(compare_list_height[index]).outerHeight()) + 'px');
			});
		});

	}

	if($('.compare__table-mobile').length) {

		var mobile_compare_list_height_1 = $('.compare__table-mobile .part-one .attr-col li');

		$('.compare__table-mobile .part-one .ft-body li').each(function( index ) {
			$(this).css('height', parseFloat($(mobile_compare_list_height_1[index]).outerHeight()) + 'px');
		});

		$(window).resize(function() {
			var mobile_compare_list_height_1 = $('.compare__table-mobile .part-one .attr-col li');

			$('.compare__table-mobile .part-one .ft-body li').each(function( index ) {
				$(this).css('height', parseFloat($(mobile_compare_list_height_1[index]).outerHeight()) + 'px');
			});
		});

		var mobile_compare_list_height_2 = $('.compare__table-mobile .part-two .attr-col li');

		$('.compare__table-mobile .part-two .ft-special li').each(function( index ) {
			$(this).css('height', parseFloat($(mobile_compare_list_height_2[index]).outerHeight()) + 'px');
		});

		$(window).resize(function() {
			var mobile_compare_list_height_2 = $('.compare__table-mobile .part-two .attr-col li');

			$('.compare__table-mobile .part-two .ft-special li').each(function( index ) {
				$(this).css('height', parseFloat($(mobile_compare_list_height_2[index]).outerHeight()) + 'px');
			});
		});

	}

	if($('.c-quick-form').length) {

		$('.c-quick-form select').styler({
			onSelectClosed: function() {
				if($('.c-quick-form #summ-styler .jq-selectbox__select-text span').length == 0) {
					$('.c-quick-form #summ-styler .jq-selectbox__select-text').append(".<span>00</span>");
				}
			}
		});

		var selects = $('.c-quick-form #summ-styler .jq-selectbox__dropdown li');

		$.each(selects, function() {
			$(this).append(".<span>00</span>");
		});

		$('.c-quick-form #summ-styler .jq-selectbox__select-text.placeholder').append(".<span>00</span>");

		$('.c-quick-form input:not(#captcha)').on('focus', function() {
			$(this).prev('label').css('display', 'none');
		});

		var inputs = $('.c-quick-form input:not([type="email"])');

		inputs.on('blur', function() {
			if($(this).val() == 0) {
				$(this).prev('label').css('display', 'block');
				$(this).addClass('required-input');
			} else {
				$(this).removeClass('required-input');
			}
		});

		var emails = $('.c-quick-form input[type="email"]');
		emails.on('blur', function(){
			val = $(this).val();
			if (val == '') {
				$(this).prev('label').css('display', 'block');
			}
			if(validateMail(val)) {
				$(this).removeClass('required-input');
			} else {
				$(this).addClass('required-input');
			}
		});

		inputs.each(function() {
			if($(this).val()!='') {
				$(this).prev('label').css('display', 'none');
			}
		});

		if (localStorage) {

			$("#summ").change(function() {
				localStorage.setItem("summ", $(this).val());
			});

			$("#summ1").change(function() {
				localStorage.setItem("summ", $(this).val());
			});

			$(".js-summ-select").change(function() {
				localStorage.setItem("summ", $(this).val());
			});

			$("#first_name").blur(function() {
				localStorage.setItem("first_name", $(this).val());
			});

			$("#last_name").blur(function() {
				localStorage.setItem("last_name", $(this).val());
			});

			$("#company").blur(function() {
				localStorage.setItem("company", $(this).val());
			});
			$("#company1").blur(function() {
				localStorage.setItem("company", $(this).val());
			});

			$("#email").blur(function() {
				localStorage.setItem("email", $(this).val());
			});

			$("#phone").blur(function() {
				localStorage.setItem("phone", $(this).val());
			});
		}

		$('#verify-by-sms').click(function(e){

			error = false;

			$('.c-quick-form input[type="text"]:visible').each(function(){

				if($(this).val() == '') {

					error = true;
					$(this).addClass('required-input');
				}

			});

			$('.c-quick-form input[type="email"]:visible').each(function(){

				//if($(this).val() == '') {
				if(!validateMail($(this).val())) {

					error = true;
					$(this).addClass('required-input');
				}

			});

			$('.c-quick-form select:visible').each(function() {

				if($(this).val() == '') {

					error = true;
					$(this).next('.jq-selectbox__select').addClass('required-input');
				}

			});

			if(error) {

				e.preventDefault();

				if( $('#form-req-error').hasClass('close') ){
					$('#form-req-error').removeClass('close');
				}

				$('#form-req-error').delay(200).queue(function(next){
					$(this).addClass('open');
					next();
					$(window).scrollTop(0);
				});
			}
		})
	}

	if($('.c-full-form').length) {

		$('.c-full-form select').styler();

		$('.c-full-form input[type="text"]').each(function() {
			if($(this).val()!='') {
				$(this).prev('label').css('display', 'none');
			}
		});

		$('.c-full-form input[type="email"]').each(function() {
			if($(this).val()) {
				$(this).prev('label').css('display', 'none');
			}
		});

		$('.c-full-form input').on('focus', function() {
			$(this).prev('label').css('display', 'none');
		});

		$('.c-full-form input:not(#company_site, [type="email"], #verify)').on('blur', function() {
			if($(this).val() == 0) {
				$(this).prev('label').css('display', 'block');
				$(this).addClass('required-input');
			} else {
				$(this).removeClass('required-input');
			}
		});

		$('.c-full-form input[type="email"]').on('blur', function(){
			val = $(this).val();
			$this = $(this);
			if (val == '') {
				$(this).prev('label').css('display', 'block');
			}
			if(validateMail(val)) {
				$(this).removeClass('required-input');
			} else {
				$(this).addClass('required-input');
			}

			// if( val !== '' && validateMail(val) == false ) {
			// 	if( $('#form-email-format-error').hasClass('close') ){
			// 		$('#form-email-format-error').removeClass('close');
			// 	}

			// 	$('#form-email-format-error').delay(200).queue(function(next){
	  //               $(this).addClass('open');
	  //               next();
	  //               $(window).scrollTop(0);
   //          	});
			// }
		});

		$('.c-full-form input#company_site').on('blur', function() {
			if($(this).val() == 0) {
				$(this).prev('label').css('display', 'block');
			}
		});

		// $('.c-full-form input#confirm_email').on('blur', function() {
		// 	email = $('.c-full-form input#email').val();
		// 	if ($(this).val()  &&  $(this).val() != email) {
		// 		if( $('#form-email-error').hasClass('close') ){
		// 			$('#form-email-error').removeClass('close');
		// 		}

		// 		$('#form-email-error').delay(200).queue(function(next){
	 //                $(this).addClass('open');
	 //                next();
	 //                $(window).scrollTop(0);
  //           	});
		// 	}
		// });

		$('.c-full-form select').on('change', function() {
			if($(this).val() == 0) {
				$(this).next('.jq-selectbox__select').addClass('required-input');
			} else {
				$(this).next('.jq-selectbox__select').removeClass('required-input');
			}
		});

		if(localStorage['summ']) {
			$('#summ').val(localStorage.getItem('summ')).trigger('refresh');
		}

		if (localStorage['first_name']) {
		  $('#first_name').val(localStorage.getItem('first_name'));
		  $('#first_name').prev('label').css('display', 'none');
		}

		if (localStorage['last_name']) {
		  $('#last_name').val(localStorage.getItem('last_name'));
		  $('#last_name').prev('label').css('display', 'none');
		}

		if (localStorage['company']) {
		  $('#company').val(localStorage.getItem('company'));
		  $('#company').prev('label').css('display', 'none');
		}

		if (localStorage['email']) {
		  $('#email').val(localStorage.getItem('email'));
		  $('#email').prev('label').css('display', 'none');
		}

		if (localStorage['phone']) {
		  $('#phone').val(localStorage.getItem('phone'));
		  $('#phone').prev('label').css('display', 'none');
		}

		if (localStorage['bus_age']) {
		  $('#bus_age').val(localStorage.getItem('bus_age')).trigger('refresh');
		}

		if (localStorage['soon']) {
		  $('#soon').val(localStorage.getItem('soon')).trigger('refresh');
		}

		if (localStorage['purpose']) {
		  $('#purpose').val(localStorage.getItem('purpose')).trigger('refresh');
		}
	}

	if($('.check').length) {

		$('.check select').styler();

		if (localStorage) {


			$("#bus_age").change(function() {
				localStorage.setItem("bus_age", $(this).val());
			});

			$("#company").blur(function() {
				localStorage.setItem("company", $(this).val());
			});

			$("#summ").change(function() {
				localStorage.setItem("summ", $(this).val());
			});

			$("#soon").change(function() {
				localStorage.setItem("soon", $(this).val());
			});

			$("#purpose").change(function() {
				localStorage.setItem("purpose", $(this).val());
			});
		}

	}

	if($('.carousel').length) {

		$('.carousel .inner').bxSlider({
			nextSelector: '.carousel-next',
		  	prevSelector: '.carousel-prev'
		});

	}

	var mobile_menu_trigger = $('.mobile-menu');
	var hamburger = $('.hamburger-icon');
		mobile_menu_trigger.click(function() {
				hamburger.toggleClass('active');
				$('html, body').toggleClass('no-scroll');
				$('.mobile-nav').slideToggle();
				return false;
		});

	if($('.c-faq-list').length) {

		$('.c-faq-list ul li').on('click', function() {
			$('.c-faq-list__inner').not($(this).children('.c-faq-list__inner')).slideUp();
			$(this).children('.c-faq-list__inner').slideToggle();
		});

	}


	if($('#contact').length) {
		$('#contact').submit(submitContact);

		function submitContact(e) {
			e.preventDefault();
			var contact_us = $(this);
			var security = 1;

			fill_error = false;
			captcha_error = false;

			$('#contact input').each(function() {

				if($(this).val() == '') {

					fill_error = true;

				}

			});

			operand1 = parseInt($('.operand1').html());
			operation = $('.operation').html();
			operand2 = parseInt($('.operand2').html());
			if(operation == '+') {
				captcha = operand1 + operand2;
			}
			if(operation == 'x') {
				captcha = operand1 * operand2;
			}
			if(operation == '-') {
				captcha = operand1 - operand2;
			}



			if($('#captcha').val() != captcha && $('#captcha').val() != '') {
				captcha_error = true;
			}

			if(captcha_error) {
				if( $('#form-captcha-error').hasClass('close') ){
					$('#form-captcha-error').removeClass('close');
				}

				$('#form-captcha-error').delay(200).queue(function(next){
	                $(this).addClass('open');
	                next();
	                $(window).scrollTop(0);
            	});
			}

			if(fill_error) {
				if( $('#form-req-error').hasClass('close') ){
					$('#form-req-error').removeClass('close');
				}

				$('#form-req-error').delay(200).queue(function(next){
	                $(this).addClass('open');
	                next();
	                $(window).scrollTop(0);
            	});
			}

			if(fill_error == false && captcha_error == false) {
				$.ajax({
					url: contact_us.attr('action') + '?ajax=true',
					type: contact_us.attr('method'),
					data: contact_us.serialize(),
					success: contactSubmitted
				});
			}
			return false;
		}

		function contactSubmitted(response) {
			if (response === 'success') {
				$('aside .disclaimer').fadeOut();
				$('.quick-form-success').fadeIn();
			} else {
				if( $('#form-send-error').hasClass('close') ){
					$('#form-send-error').removeClass('close');
				}
				$('#form-send-error').delay(200).queue(function(next){
	                $(this).addClass('open');
	                next();
	            });
			}
		}
	}




// MULTISTEP FORM

	if($('.c-full-form').length) {

		$('#about').styler();

		var current_fs,
			next_fs,
			previous_fs;

		var window_width = $(window).width();

		if(window_width > 1024) {
			$('.c-full-form').css('height', $('.c-full-form fieldset:first-child').height() + 94);
		}

		$(window).resize(function() {
			var window_width = $(window).width();

			if(window_width > 1024) {
				$('.c-full-form').css('height', $('.c-full-form fieldset:first-child').height() + 94);
			} else {
				$('.c-full-form').css('height', 'auto');
			}
		});

		$('.js-verify input[type=checkbox]').change(function() {
			vf = $('.verify-button');
			if (vf.attr('disabled') && check_verify(verify)) {
				vf.removeAttr('disabled').removeClass('disabled');
			}else{
				if(!vf.attr('disabled')) {
					vf.attr('disabled', 'disabled').addClass('disabled');
				}
			}
		});

		$('.js-next').on('click', function(e) {

			e.preventDefault();

			error = false;

			$('.c-full-form .first-step input[type="text"]:not(#company_site):visible').each(function(){

				if($(this).val() == '') {

					error = true;
					$(this).addClass('required-input');
				}

			});

			$('.c-full-form .first-step select:visible').each(function() {

				if($(this).val() == '') {

					error = true;
					$(this).next('.jq-selectbox__select').addClass('required-input');
				}

			});

			$('.c-full-form input[type="email"]').each(function(){
				val = $(this).val();
				if (val == '') {
					error = true;
					$(this).addClass('required-input');
				}
			});

			if(error) {
				if( $('#form-req-error').hasClass('close') ){
					$('#form-req-error').removeClass('close');
				}

				$('#form-req-error').delay(200).queue(function(next){
	                $(this).addClass('open');
	                next();
	                $(window).scrollTop(0);
            	});
			} else {
				current_fs = $(this).parent();

				next_fs = $(this).parent().next();

				current_fs.animate({'opacity': 0}, 800, function() {

					next_fs.animate({'opacity': 1}, 800);
					next_fs.css('z-index', '2');
					current_fs.css('z-index', '1');
					$('.c-full-form').css('height', $('.c-full-form fieldset.last').height() + 94);
					$(window).scrollTop(0);
				});
			}


		});

		$('.js-back').on('click', function(e) {

			e.preventDefault();

			current_fs = $(this).parent();

			previous_fs = $(this).parent().prev();

			current_fs.animate({'opacity': 0}, 800, function() {

				previous_fs.animate({'opacity': 1}, 800);
				previous_fs.css('z-index', '2');
				current_fs.css('z-index', 1);
				$('.c-full-form').css('height', $('.c-full-form fieldset:first-child').height() + 94);
				$(window).scrollTop(0);
			});
		});

		$('.radio-container #military').change(function() {
			$('.military-checked').fadeToggle();
			$('.military-checked select').each(function() {
				$(this).removeAttr('disabled');
			})
			$('.responders-checked').fadeToggle();
			$('.responders-checked select').each(function() {
				$(this).attr('disabled', true);
			})
			$('.responders-checked input').each(function() {
				$(this).attr('disabled', true);
			})
		});

		if($('.radio-container #military').not(':checked')) {
			$('.military-checked').fadeToggle();
			$('.military-checked select').each(function() {
				$(this).removeAttr('disabled');
			})
			$('.responders-checked').fadeToggle();
			$('.responders-checked select').each(function() {
				$(this).attr('disabled', true);
			})
			$('.responders-checked input').each(function() {
				$(this).attr('disabled', true);
			})
		};

		$('.radio-container #responders').change(function() {
			$('.responders-checked').fadeToggle();
			$('.responders-checked select').each(function() {
				$(this).removeAttr('disabled');
			})
			$('.responders-checked input').each(function() {
				$(this).removeAttr('disabled');
			})
			$('.military-checked').fadeToggle();
			$('.military-checked select').each(function() {
				$(this).attr('disabled', true);
			})
		});

		if($('.radio-container #responders').not(':checked')) {
			$('.responders-checked').fadeToggle();
			$('.responders-checked select').each(function() {
				$(this).removeAttr('disabled');
			})
			$('.responders-checked input').each(function() {
				$(this).removeAttr('disabled');
			})
			$('.military-checked').fadeToggle();
			$('.military-checked select').each(function() {
				$(this).attr('disabled', true);
			})
		};

		if( $('#credit_yes').is(':checked') ) {
			$('.card-receipts').fadeIn();
			$('#card-receipts').removeAttr('disabled');
		}

		if( $('#credit_no').is(':checked') ) {
			$('.card-receipts').fadeOut();
			$('#card-receipts').attr('disabled', true);
		}

		$('#credit_no').change(function() {
			$('#card-receipts').attr('disabled', true);
			$('.card-receipts').fadeToggle();
		});

		$('#credit_yes').change(function() {
			$('.card-receipts').fadeToggle();
			$('#card-receipts').removeAttr('disabled');
		});

		$('.js-popup').on('click', function(e) {
			e.preventDefault();
			if( $('#' + $(this).data('url')).hasClass('close') ){
				$('#' + $(this).data('url')).removeClass('close');
			}
			$('#' + $(this).data('url')).delay(200).queue(function(next){
                $(this).addClass('open');
                next();
            });
		})

		$('.verify-button').on('click', function(e) {

			e.preventDefault();
			submitForm();

		});


	}

	function submitForm() {
		var contact_form = $('#quote-form');

		error = false;

		$('.c-full-form .first-step input[type="text"]:not(#company_site, #verify):visible').each(function() {

				if($(this).val() == '') {

					error = true;
					$(this).addClass('required-input');
				}

		});

		$('.c-full-form .first-step select:visible').each(function() {

			if($(this).val() == '') {

				error = true;
				$(this).next('.jq-selectbox__select').addClass('required-input');
			}

		});

		$('.c-full-form input[type="email"]').each(function(){
			val = $(this).val();
			if (val == '') {
				$(this).prev('label').css('display', 'block');
			}
			if(validateMail(val)) {
				$(this).removeClass('required-input');
			} else {
				error = true;
				$(this).addClass('required-input');
			}
		});

		email = $('.c-full-form input#email').val();
		if ($('.c-full-form input#confirm_email').val()  &&  $('.c-full-form input#confirm_email').val() != email) {
			if( $('#form-email-error').hasClass('close') ){
				$('#form-email-error').removeClass('close');
			}

			$('#form-email-error').delay(200).queue(function(next){
                $(this).addClass('open');
                next();
                $(window).scrollTop(0);
        	});
		}

		$('.c-full-form input[type="email"]').each(function(){
			val = $(this).val();
			$this = $(this);
			if (val == '') {
				$(this).prev('label').css('display', 'block');
			}
			if(validateMail(val)) {
				$(this).removeClass('required-input');
			} else {
				$(this).addClass('required-input');
			}

			if( val !== '' && validateMail(val) == false ) {
				if( $('#form-email-format-error').hasClass('close') ){
					$('#form-email-format-error').removeClass('close');
				}

				$('#form-email-format-error').delay(200).queue(function(next){
	                $(this).addClass('open');
	                next();
	                $(window).scrollTop(0);
            	});
			}
		});


		$('.c-full-form .last input[type="text"]:not(#verify):visible').each(function() {

				if($(this).val() == '') {

					error = true;
					$(this).addClass('required-input');
				}

			});

			$('.c-full-form .last select:visible').each(function() {

				if($(this).val() == '') {

					error = true;
					$(this).next('.jq-selectbox__select').addClass('required-input');
				}

			});

			$('.c-full-form .last input[type="text"]:not(#verify):visible').each(function() {

				if($(this).val() == '') {

					error = true;

				}

			});

			$('.c-full-form .last select:visible').each(function() {

				if($(this).val() == '') {

					error = true;
				}

			});

			if(error) {
				if( $('#form-req-error').hasClass('close') ){
					$('#form-req-error').removeClass('close');
				}

				$('#form-req-error').delay(200).queue(function(next){
	                $(this).addClass('open');
	                next();
	                $(window).scrollTop(0);
            	});
		} else {
			//$('.quick-form-success').fadeIn();
			if( $('#form-sms-waiting').hasClass('close') ){
				$('#form-sms-waiting').removeClass('close');
			}

			$('#form-req-error').delay(200).queue(function(next){
				$(this).addClass('open');
				next();
			});

			 $('#quote-form').ajaxSubmit({
				// url: contact_form.attr('action') + '?ajax=true',
				// type: contact_form.attr('method'),
				dataType: 'json',
				data: { action: "verify", security: security },
				error: function(obj, status) {
					if ($('#form-send-error').hasClass('close')) {
						$('#form-send-error').removeClass('close');
					}
					$('#form-send-error').delay(200).queue(function(next) {
						$(this).addClass('open');
						next();
					});

                },
				success: function(result) {
							if (result) {
								if (result.success) {
									$('.form.c-full-form .disclaimer').fadeOut();
									//
									$('.submit').removeClass('disabled');
									$('#verify').attr('disabled', false);
								} else if (result.error) {
										if( $('#form-send-error').hasClass('close') ){
											$('#form-send-error').removeClass('close');
										}
										$('#form-send-error').delay(200).queue(function(next){
							                $(this).addClass('open');
							                next();
							            });
									} else {
										if( $('#form-send-error').hasClass('close') ){
											$('#form-send-error').removeClass('close');
										}
										$('#form-send-error').delay(200).queue(function(next){
							                $(this).addClass('open');
							                next();
							            });
									}
								} else {
									if( $('#form-send-error').hasClass('close') ){
										$('#form-send-error').removeClass('close');
									}
									$('#form-send-error').delay(200).queue(function(next){
						                $(this).addClass('open');
						                next();
						            });
								}

					// if(result) {
					// 	if(result.success) {
					// 		$('.form.c-full-form .disclaimer').fadeOut();
					// 		$('.quick-form-success').fadeIn();
					// 	} else {
					// 		if( $('#form-send-error').hasClass('close') ){
					// 			$('#form-send-error').removeClass('close');
					// 		}
					// 		$('#form-send-error').delay(200).queue(function(next){
				 //                $(this).addClass('open');
				 //                next();
				 //            });
					// 	}
					// }
				}
			});
		}
		return false;
	}


	// function formSubmitted(statusText) {
	// 	if(statusText.success) {
	// 		$('.full-form-success').fadeIn();
	// 	} else {
	// 		$('#form-send-error').delay(200).queue(function(next){
 //                $(this).addClass('open');
 //                next();
 //            });

	// 	}
	// }

function check_verify() {
	verify = true;
	$('.js-verify input[type=checkbox]').each(function() {
		if (!$(this).is(':checked')) {
			verify = false;
		}
	});
	return verify;
}

function validateMail(email){
	var reg = /.+@.+\..+$/;
	return (email && reg.test(email))
}


// Modal Code

$('.overlay-close').on('click', function(e){

	e.preventDefault();

	var parent = $(this).parents('.overlay-door').attr('id');

	$('#' + parent).removeClass('open').addClass('close').delay(800)
});

});
